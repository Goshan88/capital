package com.capital;

import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import android.widget.Toast;

//����� ������� �������� ������
public class JSONparsing {
	//������ �������� ������
	/*{"base":"RUB","date":"2017-11-13","rates":
	{"AUD":0.022005,"BGN":0.02821,"BRL":0.055332,"CAD":0.02136
	,"CHF":0.016718,"CNY":0.11165,"CZK":0.36877,"DKK":0.10733,
	"GBP":0.01284,"HKD":0.13112,"HRK":0.1089,"HUF":4.5007,"IDR
	":228.05,"ILS":0.059565,"INR":1.1004,"JPY":1.9049,"KRW":18
	.823,"MXN":0.32234,"MYR":0.070468,"NOK":0.1369,"NZD":0.024
	343,"PHP":0.86143,"PLN":0.061082,"RON":0.067159,"SEK":0.14
	093,"SGD":0.022886,"THB":0.55581,"TRY":0.065365,"USD":0.01
	6812,"ZAR":0.24475,"EUR":0.014424}}*/
	
	
	//��������� ���� 
		public static String getDate(){
			String date =null;
			
			//��������� �� ������ ��������� ������
			String rawContent = DataStorage.loadContent();
			if (rawContent.equals(iCapital.NOTHING)){
				Log.d(iCapital.TAG,"Haven't content in memory");
				return date;
			}
						
			try{
				JSONObject JSON_obj = new JSONObject(rawContent);
				date = JSON_obj.getString("date");
				Log.d(iCapital.TAG,"Json obj: " + JSON_obj.toString());
				Log.d(iCapital.TAG,"Json date result: " + date);
			}catch(Exception e){
				Toast.makeText(MyContext.getContext(), (MyContext.getContext().getResources().getString(R.string.error_parsing)), Toast.LENGTH_LONG).show();
				Log.d(iCapital.TAG,MyContext.getContext().getResources().getString(R.string.error_parsing)+" " + e.getMessage());
				return date;
			}
			
			return date;
		}
	
	public static float getRate(String Currency){
		float result=1;
		JSONObject JSON_obj = null;
		JSONObject JSON_rates = null;//����� ������ �����
		String st_result=null;
		
		//��������� �� ������ ��������� ������
		String rawContent = DataStorage.loadContent();
		if (rawContent.equals(iCapital.NOTHING)){
			Log.d(iCapital.TAG,"Haven't content in memory");
			return result;
		}
		
		//��������� ���� �������� ������ ��������� � ����������� �� ������� 1
		if(Currency.equals(getMainCurrency())){
			return 1;
		}
		
		try{//����� json ������ �� ����� ������
			JSON_obj = new JSONObject(rawContent);
		}catch(Exception e){
			Toast.makeText(MyContext.getContext(), (MyContext.getContext().getResources().getString(R.string.error_parsing)), Toast.LENGTH_LONG).show();
			Log.d(iCapital.TAG,MyContext.getContext().getResources().getString(R.string.error_parsing)+" " + e.getMessage());
			return result;
		}
		
		try{//����� json ������ �� ����� ������
			JSON_rates = JSON_obj.getJSONObject("rates");
			//Log.d(iCapital.TAG, "JSON rates:" + JSON_rates.toString());
			st_result = JSON_rates.getString(Currency); 
			Log.d(iCapital.TAG, "JSON rate:" + st_result);
		}catch(Exception e){
			Toast.makeText(MyContext.getContext(), (MyContext.getContext().getResources().getString(R.string.error_get_json_rates)), Toast.LENGTH_LONG).show();
			Log.d(iCapital.TAG,MyContext.getContext().getResources().getString(R.string.error_get_json_rates)+" " + e.getMessage());
			return result;
		}
		
		try{
			result = Float.parseFloat(st_result);
			Log.d(iCapital.TAG, Currency + " float rate:" + String.valueOf(result));
		}catch(Exception e){
			Toast.makeText(MyContext.getContext(), (MyContext.getContext().getResources().getString(R.string.error_get_float_value)), Toast.LENGTH_LONG).show();
			Log.d(iCapital.TAG,MyContext.getContext().getResources().getString(R.string.error_get_float_value)+" " + e.getMessage());
		}
		
		return result;
	}
		
	//��������� ���� 
	public static String getMainCurrency(){
	String mainCurrency ="RUB";
				
	//��������� �� ������ ��������� ������
	String rawContent = DataStorage.loadContent();
	if (rawContent.equals(iCapital.NOTHING)){
		Log.d(iCapital.TAG,"Haven't content in memory");
		return mainCurrency;
	}
							
	try{
		JSONObject JSON_obj = new JSONObject(rawContent);
		mainCurrency = JSON_obj.getString("base");
		Log.d(iCapital.TAG,"Json obj: " + JSON_obj.toString());
		Log.d(iCapital.TAG,"Json main Currency result: " + mainCurrency);
	}catch(Exception e){
		Toast.makeText(MyContext.getContext(), (MyContext.getContext().getResources().getString(R.string.error_parsing)), Toast.LENGTH_LONG).show();
		Log.d(iCapital.TAG,MyContext.getContext().getResources().getString(R.string.error_parsing)+" " + e.getMessage());
		return mainCurrency;
	}
				
	return mainCurrency;
	}
}
