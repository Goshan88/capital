package com.capital;

import java.util.Calendar;
import java.util.Date;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Settings extends Activity implements OnClickListener,OnItemSelectedListener{
	
	ImageButton Back;
	TextView DateUpdate,NameCurrency;
	String[] tmpArray;
	Spinner sCurrency;
	String currentCurrency;
	boolean ChangeMainCurrency=false;
	//public static Handler dateHandler;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.settings);
	getActionBar().hide();
    
    Back = (ImageButton) findViewById(R.id.iBack_settings);
    DateUpdate = (TextView) findViewById(R.id.tDateUpdate);
    NameCurrency = (TextView) findViewById(R.id.tNameMainCurrency);
    Back.setOnClickListener(this);
	
	drawSpinerCurrency();
	drawDate();
	
	//���������� �������
	DataStorage setData = new DataStorage();
	tmpArray = setData.getSpinerItemSpiner(this, iCapital.SAFE_MAIN_SPINER);
	currentCurrency = tmpArray[0];
	
		
	//Locate the Banner Ad in activity_main.xml
	AdView adView = (AdView) this.findViewById(R.id.adView);
	// Request for Ads
	AdRequest adRequest = new AdRequest.Builder().build();
	// Load ads into Banner Ads
	adView.loadAd(adRequest);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
			case R.id.iBack_settings:
				//����� ������ ����� ���������� � ������ � ������� ����� ����������� �������� ������
				String mainCurrency = sCurrency.getSelectedItem().toString();
				String curCurrency = DataStorage.loadMainCurrency();
				if(mainCurrency.equals(curCurrency)){
					//��� ��������� ������ �� ������
					Log.d(iCapital.TAG, "Main currency remain");
				}else{
					Log.d(iCapital.TAG, "Main currency change");
					ServiceCapital.servicHandler.sendEmptyMessage(iCapital.START_REQUEST);//���� ������� ������ �� ��� �����������
				}
				DataStorage.saveMainCurrency(mainCurrency);
			    MainView.drawResult();
			    DataStorage setData = new DataStorage();
				setData.rewriteSpinerArray(mainCurrency,iCapital.SAFE_MAIN_SPINER); 
				
			    finish();
				break;
			default:
				break;
		}
	}
	
	public void drawDate(){
		DateUpdate.setText(JSONparsing.getDate());
	}
	
	public void drawSpinerCurrency(){//������ ������
		 DataStorage setData = new DataStorage();
	     tmpArray = setData.getSpinerItemSpiner(this, iCapital.SAFE_MAIN_SPINER);
	     Log.d(iCapital.TAG,"Load currency spiner array: "+tmpArray[0] + ", "+tmpArray[1] + ", "+tmpArray[2] + ", "+tmpArray[3] + ".");
	     sCurrency = (Spinner) findViewById(R.id.sMainCurrency);
	     ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, tmpArray);
		 spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 sCurrency.setAdapter(spinnerArrayAdapter);
	     sCurrency.setOnItemSelectedListener(this);
	}

	
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
		//Log.d(iCapital.TAG,"Click on spiner: " +"Parent- " + parent.toString() +", View-" + view.toString());
		String currentCurrency = sCurrency.getSelectedItem().toString();
		NameCurrency.setText(myCurrency.getName(currentCurrency));
		if (parent.getId()==R.id.sMainCurrency){
			if(pos<(iCapital.NUMB_SEL_ITEM-1)){
		    	//���� �������� ����� ������ �� ���������� � ������������ ������
		    	if(currentCurrency.equals(tmpArray[pos])){
		    		Log.d(iCapital.TAG, " mainCurrency remain");
		    		ChangeMainCurrency=false;
		    		
		    	}else{
		    		Log.d(iCapital.TAG, " mainCurrency change");
		    		ChangeMainCurrency=true;
		    	}
		      }else{
		    	//�������� �������� � ����������� ������ � ������
		    	Intent intent = new Intent(this, SelectCurrency.class);
		    	startActivityForResult(intent,1);
		    }
	    }
	}
	
	@Override
    public void onNothingSelected(AdapterView<?> parent) {
       // sometimes you need nothing here
		Toast.makeText(parent.getContext(), 
		        "������ �� �������", Toast.LENGTH_SHORT).show();
    }
	
	@Override //��� ���� ����� ���������� �� �������� ��������� ������
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		if (data == null) {return;}
		String Currency = data.getStringExtra("currency");
		Log.d(iCapital.TAG,"Safe main currency -"+Currency);
		//��������� � ������ ����� ������
		DataStorage addData = new DataStorage();
		addData.rewriteSpinerArray(Currency,iCapital.SAFE_MAIN_SPINER);
		//���������� ������
		drawSpinerCurrency();
	}
}
