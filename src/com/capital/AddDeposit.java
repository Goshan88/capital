package com.capital;
import java.util.Currency;

import com.capital.validators.BaseValidator.ValidationMode;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.capital.validators.EditTextValidator;
import com.capital.validators.LengthChecker;
import com.capital.validators.NotEmptyChecker;
import com.capital.validators.ValidationSummary;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class AddDeposit extends Activity implements OnItemSelectedListener,OnClickListener{

    
    String[] tmpArray;
    TextView tValue;
    String cur_symbol = "";//���������� ������������ ��� ��������� �������� �� �����
    String cur_currency;
    ImageButton iBack, iEnter;
    EditText eNameSum,eSum;
    TextView tvTitle,tvErrorView;
    Spinner sCurrency;
    float Value;
    int DepositNumb=0;
    boolean changeDeposit=false;
    protected boolean first_call=true;
    ValidationSummary summaryValidator_;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	 setContentView(R.layout.add);
     getActionBar().hide();
     
     
     tvErrorView = (TextView) findViewById(R.id.erCurrency);
     tvTitle = (TextView) findViewById(R.id.tTitle);
     iBack = (ImageButton) findViewById(R.id.iBack_add);
     iEnter = (ImageButton) findViewById(R.id.iEnter);
     eNameSum = (EditText)findViewById(R.id.eNameSum);
     eSum = (EditText)findViewById(R.id.eSum);
     
     iBack.setOnClickListener(this);
     iEnter.setOnClickListener(this);
     
     drawSpiner();
     //���� ���������� ������ �� ������� ����� ������ ����������
    
     Intent intent = getIntent();
     Log.d(iCapital.TAG_ADD,"Get intent: " +intent.toString());
      if(intent.hasExtra(iCapital.EXTRA_CURRENCY) == true){
    	 changeDeposit=true;
    	 String Deposit_Name = intent.getStringExtra(iCapital.EXTRA_DEPOSIT_NAME);
    	 String Deposit_Numb = intent.getStringExtra(iCapital.EXTRA_NUMB);
    	 String cur_currency = intent.getStringExtra(iCapital.EXTRA_CURRENCY);
    	 String addValue = intent.getStringExtra(iCapital.EXTRA_VALUE);
    	  try{
    		  DepositNumb = Integer.parseInt(Deposit_Numb);
		}catch(Exception e){
			Toast.makeText(this, (getResources().getString(R.string.Err_parsing_int)), Toast.LENGTH_LONG).show();
		 }
    	 
		 Log.d(iCapital.TAG,"Extra data:"+Deposit_Name+","+Deposit_Numb+","+cur_currency+","+addValue+".");
		 
		 tvTitle.setText(getResources().getString(R.string.change_deposit));
		 eNameSum.setText(Deposit_Name);
		 
		 eSum.setText(addValue);
		 
		 DataStorage addData = new DataStorage();
		 addData.rewriteSpinerArray(cur_currency,iCapital.SAFE_SPINER);//��������� ������
		 drawSpiner();
     	}else{
     		DepositNumb = 0;
     		changeDeposit=false;
     		tvTitle.setText(getResources().getString(R.string.add_deposit));
     	}
      
      //���������� ����������� � ����� �������� � �����
      //������� ������������ ���������
  	  summaryValidator_ = new ValidationSummary(getString(R.string.common_error_message), tvErrorView);
  	  //������� ��������� ��� ���� ����� �����
  	  EditTextValidator DepositValidator = new EditTextValidator();
  	  //��������� ��� EditText ��� ���������, ������ ������ ����� �������� (�� ����� ���������, ����� ������������ ������ ������� �����)
  	  DepositValidator.setViewToValidate(eNameSum, ValidationMode.Auto);
  	  //���������� TextView ��� ����������� ��������� �� ������� � ���, � �� � ������� ����������� ����
  	  DepositValidator.setExternalErrorView( (TextView) findViewById(R.id.erCurrency));
  	  //�������� �������� �� ������� ������
  	  DepositValidator.addConditionChecker(new NotEmptyChecker( getString(R.string.error_empty_string) ));
  	  //� �� �����
  	  DepositValidator.addConditionChecker(new LengthChecker(1, iCapital.MAX_LENHT_NAME_DEPOSIT, getString(R.string.max_lent_name_deposit) ));
  	  //�������� ��������� � ������
  	  summaryValidator_.addValidator(DepositValidator);
      
  	  EditTextValidator SumValidator = new EditTextValidator();
	  SumValidator.setViewToValidate(eSum, ValidationMode.Auto);
	  SumValidator.setExternalErrorView( (TextView) findViewById(R.id.erCurrency));
	  SumValidator.addConditionChecker(new NotEmptyChecker( getString(R.string.error_numb_empty_string) ));
	  SumValidator.addConditionChecker(new LengthChecker(1, iCapital.MAX_LENHT_SUM, getString(R.string.max_lent_sum) ));
	  summaryValidator_.addValidator(SumValidator);
  	  
  	  
      	//Locate the Banner Ad in activity_main.xml
    	AdView adView = (AdView) this.findViewById(R.id.adView);
    	// Request for Ads
    	AdRequest adRequest = new AdRequest.Builder().build();
    	// Load ads into Banner Ads
    	adView.loadAd(adRequest);
	} 
	

	public void drawSpiner(){//������ ������
		 DataStorage newData = new DataStorage();
	     tmpArray = newData.getSpinerItemSpiner(this, iCapital.SAFE_SPINER);
	     Log.d(iCapital.TAG,"Load spiner array: "+tmpArray[0] + ", "+tmpArray[1] + ", "+tmpArray[2] + ", "+tmpArray[3] + ".");
	     sCurrency = (Spinner) findViewById(R.id.sCurrency);
	     ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, tmpArray);
		 spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 sCurrency.setAdapter(spinnerArrayAdapter);
	     sCurrency.setOnItemSelectedListener(this);
	}
	
	@Override
	public void onClick(View V){
		String Command = "";
		summaryValidator_.performCheck();
		switch(V.getId()){
		case R.id.iBack_add:
			Log.d(iCapital.TAG,"Press button Back");
				finish();
			break;
		case R.id.iEnter:
			Log.d(iCapital.TAG,"Press button Enter");
			if(summaryValidator_.isCorrect()){
				save_result();
			}
			break;
		default:
			break;
		}
	}
	
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
	  
		Log.d(iCapital.TAG,String.valueOf(pos)+" Array:"+tmpArray[pos]);
		
	    if(pos<(iCapital.NUMB_SEL_ITEM-1)){
	    	cur_currency = tmpArray[pos];
	    }else{
	    	//�������� �������� � ����������� ������ � ������
	    	Intent intent = new Intent(this, SelectCurrency.class);
	    	startActivityForResult(intent,1);
	    }
	}
	
	@Override
    public void onNothingSelected(AdapterView<?> parent) {

       // sometimes you need nothing here
		Toast.makeText(parent.getContext(), 
		        "������ �� �������", Toast.LENGTH_SHORT).show();
    }
	
		
	@Override //��� ���� ����� ���������� �� �������� ��������� ������
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		if (data == null) {return;}
		String Currency = data.getStringExtra("currency");
		Log.d(iCapital.TAG,"AddDeposite get currency code -"+Currency);
		//��������� � ������ ����� ������
		DataStorage addData = new DataStorage();
		addData.rewriteSpinerArray(Currency,iCapital.SAFE_SPINER);
		//���������� ������
		drawSpiner();
	}
	
	
	private void save_result(){
		String tmp_str = eSum.getText().toString();
		int deposit_sum = 0;
		try{
			deposit_sum = Integer.parseInt(tmp_str);
		}catch(Exception e){
			Toast.makeText(this, (getResources().getString(R.string.error_numb_empty_string)), Toast.LENGTH_LONG).show();
			Log.d(iCapital.TAG,getString(R.string.error_numb_empty_string) + " " + e.getMessage());
		}
		Log.d(iCapital.TAG,"add save_result ��������:" + eNameSum.getText().toString() + ", ������:" +cur_currency +
				", �����:" + String.valueOf(deposit_sum) + ", ��������:" + String.valueOf(changeDeposit));
		Deposit safeDeposit = new Deposit(eNameSum.getText().toString(), cur_currency,deposit_sum);
		DataStorage addData = new DataStorage();
		if	(changeDeposit==true){
			//��������� �������� ����
			addData.changeDeposit(safeDeposit,DepositNumb);//��������� ����� ����
			Log.d (iCapital.TAG,"Deposit is changed");
		}else{
			addData.addDeposit(safeDeposit);//��������� ����� ����
			Log.d(iCapital.TAG,"Deposit is saved");
		}
		//������������ 	������
		addData.rewriteSpinerArray(cur_currency.toString(),iCapital.SAFE_SPINER);
		//ServiceCapital.servicHandler.sendEmptyMessage(iCapital.START_REQUEST);
		//MainActivity.mainHandler.sendEmptyMessage(iCapital.SET_RESULT);
		finish();
	}
}
	

