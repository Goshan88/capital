package com.capital;

import android.content.Context;
import android.util.Log;

public class myCurrency {
	private static String[][] myCurrency_arr = new String[iCapital.CURRENCY_TYPE][iCapital.CURRENCY_ARRAY_SIZE];
	
	myCurrency(Context mContext){
		String[] myArr_Code = mContext.getResources().getStringArray(R.array.Currency_Code);
		String[] myArr_Name = mContext.getResources().getStringArray(R.array.Currency_Name);
		String[] myArr_Symbol = mContext.getResources().getStringArray(R.array.Currency_Symbol);
		for(int i=0; i<myArr_Code.length; i++){
			myCurrency_arr[0][i] = myArr_Code[i];
			myCurrency_arr[1][i] = myArr_Name[i];
			myCurrency_arr[2][i] = myArr_Symbol[i];
			Log.d(iCapital.TAG,"MyArr -" + myArr_Code[i] + " - "  + myArr_Name[i] + " - " +  myArr_Symbol[i]);
		}
	}
	
	String[] getArrCurrency_Code(){
		return myCurrency_arr[0];
	}
	
	String[] getArrCurrency_Name(){
		return myCurrency_arr[1];
	}
	
	String[] getArrCurrency_Symbol(){
		return myCurrency_arr[2];
	}
	
	public static String getSymbol(String mCurrency){
		for(int i=0;i<myCurrency_arr[0].length;i++){
			if(mCurrency.equals(myCurrency_arr[0][i])){
				return myCurrency_arr[2][i];
			}
		}
		return "$";
	}
	
	public static String getName(String mCurrency){
		for(int i=0;i<myCurrency_arr[0].length;i++){
			if(mCurrency.equals(myCurrency_arr[0][i])){
				return myCurrency_arr[1][i];
			}
		}
		return "���������� �����";
	}
}
