package com.capital;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class DataStorage extends Activity {
	
	SharedPreferences sPref;
	static SharedPreferences st_sPref;
	
	private int count_record=0;
	
	private final String KEY_COUNT_RECORD="count_record";
	private final String KEY_DEPOSITE = "key_deposite";
	private static final String ACTIVITY_NAME = "DataStorage";
	private final String KEY_NAME = " name"; 
	private final String KEY_CURRENCY = " currency";
	private final String KEY_VALUE = " value"; 
	//private final String KEY_PERIOD_REQUEST ="period_request";
	
	DataStorage(){
		Log.d(iCapital.TAG,"DataStorage is init");
		count_record = loadCount();
	 }	
		
	 public void setContent(){
	 }
	 
	public Deposit[] init(){	
		//���������� �� ����� � ������ ������
		boolean beforeDataLoad = loadHaveData();
		//������ ������� ������ �������� �� �������� ������
		count_record = loadCount();
		
		if (beforeDataLoad==false){//������ ��� ���������
							 //��������� �������������		
			count_record = 4;
			Deposit[] DepositList = new Deposit[count_record]; 
			DepositList[0]= new  Deposit("UpWork","USD",68);
			DepositList[1]= new  Deposit("Payoneer","GBP",83);
			DepositList[2]= new  Deposit("AdMod","USD",179);
			DepositList[3]= new  Deposit("Deutche Bank","EUR",3000);
			//��������� � ������ ������ �� ���������
			DataStorage.saveMainCurrency("RUB");
			for(int i=0; i < DepositList.length; i++){
				Log.d(iCapital.TAG,"Deposit[] init() "+DepositList[i].getName() +" " + DepositList[i].getCurrency().toString() +" " + String.valueOf(DepositList[i].getValue()));
				saveDeposit(DepositList[i],i);
			}
			saveCount(count_record);
			DataStorage.saveContent(iCapital.CONTENT_EXAMPLE,MyContext.getContext());
		}	
		
			//������ �� ������ ������
			Deposit[] DepositLoad = new Deposit[count_record];
			for(int i=0;i<count_record;i++){
				DepositLoad[i] = loadDeposit(i);
				Log.d(iCapital.TAG,"Deposit[] init() " +DepositLoad[i].getName() +" " + DepositLoad[i].getCurrency().toString() +" " + String.valueOf(DepositLoad[i].getValue()));
			}
			
			return DepositLoad;
	}
	
	public void saveDeposit(Deposit mDeposit, int pos){
		sPref = MyContext.getContext().getSharedPreferences(ACTIVITY_NAME,MyContext.getContext().MODE_PRIVATE);
		Editor eDeposit = sPref.edit();
		
		String key = KEY_DEPOSITE + String.valueOf(pos);			//������ ������ �� ������� ����� �������� � ��������� ������
		Log.d(iCapital.TAG,"Deposite save: key-"+key +", Name-"+ mDeposit.getName() + ", Currency-" + mDeposit.getCurrency().toString()+ ", Value-" + String.valueOf(mDeposit.getValue()));
		
		eDeposit.putString(key+KEY_NAME,mDeposit.getName());
		eDeposit.putString(key+KEY_CURRENCY,mDeposit.getCurrency().toString());
		eDeposit.putInt(key+KEY_VALUE,mDeposit.getValue());
		
		eDeposit.commit();
		Log.d(iCapital.TAG,"Save Deposit"+" -"+String.valueOf(pos));
	}
	
	public void saveNewCourse(String mMainCurrency,String[] mArrCurrency, String[] mArrValue, int CountCurrency, Context mContext){
		//Context mainContext = getApplicationContext();
		sPref = mContext.getSharedPreferences(ACTIVITY_NAME,mContext.MODE_PRIVATE);
		Editor eDeposit = sPref.edit();
		Log.d(iCapital.TAG,"Course save count save:"+ String.valueOf(CountCurrency));
		for(int i=0;i<CountCurrency;i++){
			String key = iCapital.KEY_COURSE + mArrCurrency[i] + "/" + mMainCurrency;			//������ ������ �� ������� ����� �������� � ��������� ������
			Log.d(iCapital.TAG,"Course save:key-"+key + ", Currency-" + mArrCurrency[i] +
								", mainCurrency-" + mMainCurrency + ", Value-"+ mArrValue[i]);
			eDeposit.putString(key,mArrValue[i]);
		}
				
		eDeposit.commit();
	}
	
	
	public static void saveContent(String mContent, Context mContext){
		st_sPref = mContext.getSharedPreferences(iCapital.CONTENT_SAVE,mContext.MODE_PRIVATE);
		Editor eContent = st_sPref.edit();
		Log.d(iCapital.TAG,"Content save in memory");
		eContent.putString(iCapital.CONTENT_KEY,mContent);
		eContent.commit();
	}
	
	public static String loadContent(){
		st_sPref = MyContext.getContext().getSharedPreferences(iCapital.CONTENT_SAVE,MyContext.getContext().MODE_PRIVATE);
		String mContent = st_sPref.getString(iCapital.CONTENT_KEY,iCapital.NOTHING);
		Log.d(iCapital.TAG,"Content load from memory");
		return mContent;
	}
	
	public void saveDate(String mMainCurrency){
		sPref = MyContext.getContext().getSharedPreferences(ACTIVITY_NAME,MyContext.getContext().MODE_PRIVATE);
		Editor eDeposit = sPref.edit();
		
		Date date_new = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date_new);
		String saveDateFormat = String.valueOf(cal.get(Calendar.MONTH)+1) + "/" //������ �� ������ ������ �� ����� �����
								+String.valueOf(cal.get(Calendar.DAY_OF_MONTH)) +"/"
								+String.valueOf(cal.get(Calendar.YEAR) +"/"
								+String.valueOf(cal.get(Calendar.HOUR_OF_DAY))+"/"
								+String.valueOf(cal.get(Calendar.MINUTE))+"/"
								+String.valueOf(cal.get(Calendar.SECOND)));
		Log.d(iCapital.TAG,"save Date:" + saveDateFormat);
		
		
		eDeposit.putString(iCapital.KEY_DATE_UPDATE+mMainCurrency,saveDateFormat);
		
		eDeposit.commit();
	}
	
	public String[] LoadCourse(String mMainCurrency, String[] mArrCurrency, int CountCurrency){
		String[] Result = new String[CountCurrency];
		
		sPref = MyContext.getContext().getSharedPreferences(ACTIVITY_NAME,MyContext.getContext().MODE_PRIVATE);
		Editor eDeposit = sPref.edit();
		
		for(int i=0;i<CountCurrency;i++){
			String key = iCapital.KEY_COURSE + mArrCurrency[i] + "/" + mMainCurrency;			//������ ������ �� ������� ����� �������� � ��������� ������
			
			Result[i] = sPref.getString(key,iCapital.NOTHING);
			Log.d(iCapital.TAG,"Load Course:" + mArrCurrency[i] + "/" + mMainCurrency + Result[i]);
		}
		return Result;
	}
	
	public Date loadDate(String mMainCurrency){
		Date mDate=null;
		sPref = MyContext.getContext().getSharedPreferences(ACTIVITY_NAME,MyContext.getContext().MODE_PRIVATE);
		
		String sDate = sPref.getString(iCapital.KEY_DATE_UPDATE+mMainCurrency, iCapital.NOTHING);
		if(sDate.equals(iCapital.NOTHING)){
			Log.d(iCapital.TAG,"No save Date: ");
			return mDate;
		}
		
		SimpleDateFormat dateFormat = new SimpleDateFormat(iCapital.DATE_SIMPLE_FORMAT);
		try{
				mDate = dateFormat.parse(sDate);
		}catch(Exception e){
			Log.d(iCapital.TAG,"Error parsing load Date: " + e.getMessage());
			return mDate;
		}
		
		Log.d(iCapital.TAG,"Load date: "+mDate.toString());
		return mDate;
	}
	
	public Deposit loadDeposit(int pos){
		
		String Name=null, nameCurrency=null;
		
		sPref = MyContext.getContext().getSharedPreferences(ACTIVITY_NAME,MyContext.getContext().MODE_PRIVATE);
		String key = KEY_DEPOSITE + String.valueOf(pos);			//������ ������ �� ������� ����� �������� � ��������� ������
		Log.d(iCapital.TAG,"loadString_key "+key);
				
		Name = sPref.getString(key + KEY_NAME, "Deposit");
			
		nameCurrency = sPref.getString(key + KEY_CURRENCY, "USD");
			
		int Value=sPref.getInt(key + KEY_VALUE, 100);
			
		Deposit tmpDeposit= new Deposit(Name,nameCurrency,Value);
		Log.d(iCapital.TAG,tmpDeposit.getName()+" "+tmpDeposit.getCurrency().toString()+" " + String.valueOf(tmpDeposit.getValue()));
		
		return tmpDeposit;
	}
	
	public void deleteDeposite(int pos){
		Deposit[] tmpDeposit= init();
		Deposit[] resDeposit= new Deposit[(tmpDeposit.length-1)];
		int count=0;
		for(int i=0;i<tmpDeposit.length;i++){
			if (i!=pos){
				resDeposit[count] = tmpDeposit[i];
				count = count + 1;
			}
		}
		//��������� ������� ���������
		saveCount(resDeposit.length);
		//��������� ������
		for(int i=0;i<resDeposit.length;i++){
			saveDeposit(resDeposit[i],i);
		}
	}
	
	public void saveCount(int count){
		sPref = MyContext.getContext().getSharedPreferences(ACTIVITY_NAME,MyContext.getContext().MODE_PRIVATE);
		Editor ed = sPref.edit();		
		ed.putInt(KEY_COUNT_RECORD, count);
		ed.commit();
		Log.d(iCapital.TAG,"Save count Deposit " + String.valueOf(count));
	}
	
	
	public int loadCount(){
		int count=0;
		sPref = MyContext.getContext().getSharedPreferences(ACTIVITY_NAME,MyContext.getContext().MODE_PRIVATE);
		count = sPref.getInt(KEY_COUNT_RECORD, 0);
		Log.d(iCapital.TAG,"Load count Deposit " + String.valueOf(count));
		return count;
	}
	
	private boolean loadHaveData(){
		boolean have_data=false;
		sPref = MyContext.getContext().getSharedPreferences(ACTIVITY_NAME,MyContext.getContext().MODE_PRIVATE);
		have_data = sPref.getBoolean(iCapital.EXTRA_DATA_HAVE, false);
		Log.d(iCapital.TAG,"Load HaveDate" + String.valueOf(have_data));
		if(have_data==false){
			//������ �� ����� �� ������������
			Editor ed = sPref.edit();		
			ed.putBoolean(iCapital.EXTRA_DATA_HAVE, true);
			ed.commit();
		}
		return have_data;
	}
	
	
	static public void saveMainCurrency(String mCurrency){
		st_sPref = MyContext.getContext().getSharedPreferences(ACTIVITY_NAME,MyContext.getContext().MODE_PRIVATE);
		Editor ed = st_sPref.edit();		
		ed.putString(iCapital.KEY_MAIN_CURRENCY, mCurrency);
		ed.commit();
		Log.d(iCapital.TAG,"Save main currency " + mCurrency);
	}
	
	static public String loadMainCurrency(){
		String main_cur="RUB";
		st_sPref = MyContext.getContext().getSharedPreferences(ACTIVITY_NAME,MyContext.getContext().MODE_PRIVATE);
		main_cur = st_sPref.getString(iCapital.KEY_MAIN_CURRENCY, iCapital.NOTHING);
		if (main_cur.equals( iCapital.NOTHING)){
			main_cur = "RUB";//���� ������ ��� ���������
			Log.d(iCapital.TAG,"Load default main currency " + main_cur);
			saveMainCurrency(main_cur);
		}
		Log.d(iCapital.TAG,"Load main currency " + main_cur);
		return main_cur;
	}
	
	
	public void addDeposit(Deposit mDeposit){
		saveDeposit(mDeposit,count_record);
		count_record = count_record + 1;
		saveCount(count_record);
	}
	
	public void changeDeposit(Deposit mDeposit, int mNumb){
		saveDeposit(mDeposit,mNumb);
	}
		
	public String[] initSpinArray(String mKey){//������������� ������� ��� �������
		boolean init_array=false;
		String Array[]=loadSpinerArray(mKey);//������ ������
				
		for(int i=0;i<iCapital.MAX_SPINER_ITEM;i++){ //���� ���� ���� ������� ������� ����� null �� ������������������ ���
			if (Array[i]==null){
				init_array = true;
				Log.d(iCapital.TAG, "Load array spiner error element-" + String.valueOf(i));
			}
		}
		
		if(init_array == true){
			Log.d(iCapital.TAG, "Initialize spinr array");
			saveSpinerArray(iCapital.Spiner_Array,mKey);
			Array=loadSpinerArray(mKey);
		}
		return Array;
	}
	
	public String[] loadSpinerArray(String NameArr){
		String key;
		String Array[]=new String[iCapital.MAX_SPINER_ITEM];
		//������ ������ ������� �������, ���� �� ������� �� ���������� �������
		sPref = MyContext.getContext().getSharedPreferences(ACTIVITY_NAME,MyContext.getContext().MODE_PRIVATE);
		Log.d(iCapital.TAG, "Load array spiner");
		for(int i=0;i<iCapital.MAX_SPINER_ITEM;i++){ //�������� ������
			key = NameArr + String.valueOf(i);
			Array[i] = sPref.getString(key, null);
		}
		Log.d(iCapital.TAG,"LoadSpinerArray: " +Array[0]+"."+Array[1]+"."+Array[2]);
		return Array;
	}
	
	public void saveSpinerArray(String[] mArray, String NameArr){
		String key;
		//������ ������ ������� �������, ���� �� ������� �� ���������� �������
		sPref = MyContext.getContext().getSharedPreferences(ACTIVITY_NAME,MyContext.getContext().MODE_PRIVATE);
		Log.d(iCapital.TAG, "Safe spiner array");
		Editor ed = sPref.edit();
		for(int i=0;i<mArray.length;i++){ //load ������
			key = NameArr + String.valueOf(i);
			ed.putString(key, mArray[i]);
		}
		ed.commit();
		Log.d(iCapital.TAG,"SafeSpinerArray: " +mArray[0]+"."+mArray[1]+"."+mArray[2]);
	}
	
	public void rewriteSpinerArray(String Name, String safKey){//������������ ������, ��������� ������� �� ������ �������
											  //���� �������� ���� �� �� ������� ��������� ������� �� �������
		String[] Array=loadSpinerArray(safKey);
		String[] tmpArray = new String[Array.length];
		boolean element_have = false;
		int numb_el=0;
		
		for(int i=0;i<Array.length;i++){
			if(Name.equals(Array[i])){
				element_have = true;
				numb_el = i;
			}
		if(element_have==true){
			//���� ������� ��� ���� � ������� ����������� ������
			if(numb_el == 0) return;//������ �� ������ ��� ������
			tmpArray[0] = Name;
			if(numb_el == 1){
				tmpArray[1] = Array[0];
				tmpArray[2] = Array[2];
			}else{
				tmpArray[1] = Array[0];
				tmpArray[2] = Array[1];
			}
		}else{
			//���� �������� �� ���� � ������ �� ������� ���������
			tmpArray[0] = Name;
			tmpArray[1] = Array[0];
			tmpArray[2] = Array[1];
		}
			
		}
		Log.d(iCapital.TAG,"RewriteSpinerArray: " +Array[0]+"."+Array[1]+"."+Array[2]+"=="+String.valueOf(element_have)+Name+String.valueOf(numb_el)+"=="+tmpArray[0]+"."+tmpArray[1]+"."+tmpArray[2]);
		saveSpinerArray(tmpArray,safKey);
	}
	
	
	public String[] getSpinerItemSpiner(Context mContext,String SpinerName){//������ ������
		 DataStorage addData = new DataStorage();
	     String[] tmpArray = addData.initSpinArray(SpinerName);
	     String[] result = new String[iCapital.SpinerArray.length];
	     for(int i=0;i<tmpArray.length;i++){
	    	 result[i] = tmpArray[i];
	     }
	     result[(iCapital.SpinerArray.length-1)]=iCapital.SpinerArray[(iCapital.SpinerArray.length-1)];
		
		return result;
	}
}
