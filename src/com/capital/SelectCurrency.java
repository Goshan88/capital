package com.capital;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class SelectCurrency extends Activity implements OnClickListener{

	final String ATTRIBUTE_CUR_NAME_TEXT = "name";
	final String ATTRIBUTE_CUR_CODE_TEXT = "code";
	final String ATTRIBUTE_CUR_SYMBOL_TEXT = "symbol";
	
	ArrayList<Map<String, Object>> data;
	Map<String, Object>m;
	
	SimpleAdapter sAdapter;
	EditText tSearch;
	ImageButton Back;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.select_currency);
	getActionBar().hide();
    
    ListView lvCurrencyList =(ListView)findViewById(R.id.lvCurrencyList);
    tSearch = (EditText)findViewById(R.id.tSearch);
    Back = (ImageButton) findViewById(R.id.iBack_select);
   	Back.setOnClickListener(this);

    data = new ArrayList<Map<String,Object>>();
    
    String[] tmp_arr_name = getResources().getStringArray(R.array.Currency_Name);
    String[] tmp_arr_code = getResources().getStringArray(R.array.Currency_Code);
    
    	//���� ������� � ��� ������ 19 �� ������ �������� ������ �� ����������
    	String tmp=null;
    	for(int i=0; i<tmp_arr_code.length;i++){
    		m = new HashMap<String, Object>();
    		m.put(ATTRIBUTE_CUR_NAME_TEXT,tmp_arr_name[i]);
			m.put(ATTRIBUTE_CUR_CODE_TEXT,tmp_arr_code[i]);
			data.add(m);
			Log.d(iCapital.TAG,"Make ListItem: "+tmp_arr_name[i] +", " + tmp_arr_code[i]);
    	}
    	
	String[] from = {ATTRIBUTE_CUR_NAME_TEXT, ATTRIBUTE_CUR_CODE_TEXT};
	int[] to = {R.id.tNameCurrency,R.id.tShortNameCurrency};
	sAdapter = new SimpleAdapter(this, data, R.layout.item_selectactivity, from, to);
	lvCurrencyList.setAdapter(sAdapter);
	
	lvCurrencyList.setOnItemClickListener(new OnItemClickListener(){//���������� ������� �� ������
			public void onItemClick(AdapterView<?> parent, View view, int position,long id){
				Object item = parent.getItemAtPosition(position);
				String sel_item = item.toString();
				Log.d(iCapital.TAG,"itemClick:position=" + position + ", id=" + id + ", item=" + sel_item);
				//���������� �� ������ ��� ������
				String Currency = sel_item.substring((sel_item.length()-4), (sel_item.length()-1));
				Log.d(iCapital.TAG,"Currency is selected=" + Currency);
				//���������� �������� ������ ����� � �������� ���������� ������
				Intent intent = new Intent();
				intent.putExtra("currency", Currency);
				setResult(RESULT_OK, intent);
				finish();
			}
	});
	
	
	//Locate the Banner Ad in activity_main.xml
	AdView adView = (AdView) this.findViewById(R.id.adView);
	// Request for Ads
	AdRequest adRequest = new AdRequest.Builder().build();

	// Load ads into Banner Ads
	adView.loadAd(adRequest);

	
	
	tSearch.addTextChangedListener(new TextWatcher() {//���������� ����� ��������� � EditText
	     
	    @Override
	    public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
	        // �����, ���� �������� ����� �� ��������
	        SelectCurrency.this.sAdapter.getFilter().filter(cs);
	    }
	     
	    @Override
	    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
	            int arg3) {
	        // TODO Auto-generated method stub
	         
	    }
	     
	    @Override
	    public void afterTextChanged(Editable arg0) {
	        // TODO Auto-generated method stub                          
	    }
	});
	
	tSearch.setOnFocusChangeListener(new OnFocusChangeListener() {
		@Override
		public void onFocusChange(View v, boolean hasFocus) {
		    	tSearch.setText("");
		    }
		});
    	
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
			case R.id.iBack_select:
				finish();
				break;	
			default:
				break;
		}
	}
	
}

