package com.capital;


import java.util.Currency;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;


public class MainActivity extends Activity  implements OnClickListener,OnItemClickListener{
    /** Called when the activity is first created. */
	
	ImageButton bAdd,bSetting,bHelp;
	public TextView tResult;
	DataStorage MainData;
	final  int numb_el=9;
	ListView lvMain;
	int PositionClick=0;
	DialogFragment dlg;
	public static Handler  mainHandler;
	Context mainContext;
	private InterstitialAd interstitial;
	public static myCurrency eCurrency;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        getActionBar().hide();
        
        //������������ ������
        eCurrency = new myCurrency(this);
        
        MyContext tmpContext = new MyContext(this);
        MainData = new DataStorage();
        mainContext = this;
        
        bAdd = (ImageButton)findViewById(R.id.bAdd);
        bSetting = (ImageButton)findViewById(R.id.bSetting);
        bHelp = (ImageButton)findViewById(R.id.bHelp);
        tResult = (TextView)findViewById(R.id.tResult);
        
        bAdd.setOnClickListener(this);
        bSetting.setOnClickListener(this);
        bHelp.setOnClickListener(this);
        tResult.setOnClickListener(this);
        
        lvMain =(ListView)findViewById(R.id.lvMain);
        drawMainList();
        lvMain.setOnItemClickListener(this);
        
        
		//Locate the Banner Ad in activity_main.xml
		AdView adView = (AdView) this.findViewById(R.id.adView);
		// Request for Ads
		AdRequest adRequest = new AdRequest.Builder().build();
 
		// Load ads into Banner Ads
		adView.loadAd(adRequest);
		        
        mainHandler = new Handler(){
        	  public void handleMessage(android.os.Message msg) {
        		  if (msg.what == iCapital.REDRAW_LISTVIEW){
        			  MainData.init();
        			  drawMainList();		// ��������� Listview
        			  MainView.drawResult();//���������
        		   }
        	};
        };
        startService(new Intent(this, ServiceCapital.class));//��������� ������
	}    
      
	
	public void displayInterstitial() {
		// If Ads are loaded, show Interstitial else show nothing.
		if (interstitial.isLoaded()) {
			interstitial.show();
		}
	}
		
	public void drawMainList(){
		Deposit[]  DepositList = MainData.init();
		MainView Data_out = new MainView(this,DepositList);
        Data_out.view(lvMain);
	}
	
		
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,long id){
		Object item = parent.getItemAtPosition(position);
		String sel_item = item.toString();
		Log.d(iCapital.TAG,"Deposite:position=" + position + ", id=" + id + ", item=" + sel_item+",");
		int first = sel_item.indexOf(",");
		String Deposit = sel_item.substring(6,first);
		first = sel_item.indexOf(".");
		String tmp_Count =  sel_item.substring(first,sel_item.length());//count cut
		first = tmp_Count.indexOf("=");
		String tmp_Value =  tmp_Count.substring(first,tmp_Count.length());//value cut
		int second = tmp_Value.indexOf(",");
		String ValueSymbol =  tmp_Value.substring(1,second);//value with Symbol
		String tmpCurrency = tmp_Value.substring((second+1),tmp_Value.length());
		
		first = tmpCurrency.indexOf("=")+1;
		second = tmpCurrency.indexOf("}");
		String sCurrency =  tmpCurrency.substring(first,second);//value
		
		int SymbolLength = Currency.getInstance(sCurrency).getSymbol().length();
		String Value = ValueSymbol.substring(0,ValueSymbol.length()-SymbolLength);
		
		Log.d(iCapital.TAG,"Send to Dialog lValue-" + Value + "; " + "lCurrency-" + sCurrency + " LengthSymbol="+String.valueOf(SymbolLength));

		dlg = new Dialog(Deposit,position,this,mainHandler,sCurrency,Value);
		dlg.show(getFragmentManager(), "dlg");
	}
	
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.bAdd:
				Intent intent_add = new Intent(this, AddDeposit.class);
				startActivity(intent_add);
				break;
			case R.id.tResult:
			case R.id.tValue:
			case R.id.bSetting:
				Intent intent_settings = new Intent(this, Settings.class);
				startActivity(intent_settings);
				break;
			case R.id.bHelp:
				Intent intent_help = new Intent(this, Help.class);
				startActivity(intent_help);
				break;		
			default:
				break;
		}
	}
	
	@Override
	protected void onRestart() {
		super.onRestart();
		Log.d(iCapital.TAG, "MainActivity: onRestart()");
		drawMainList();
		MainView.drawResult();
	}
	
}