package com.capital;

public interface iCapital {
	public static String[] CurrencyCode={
		"AED"," AFN"," ALL"," AMD"," ANG"," AOA"," ARS"," AUD"," AWG"," AZN"," BAM"," BBD"," BDT"," BGN"," BHD"," BIF"," BMD"," BND"," BOB"," BRL"," BSD"," BTN"," BWP"," BYR"," BZD"," CAD"," CDF"," CHF"," CLP"," CNY"," COP"," CRC"," CUC"," CUP"," CVE"," CZK"," DJF"," DKK"," DOP"," DZD"," EGP"," ERN"," ETB"," EUR"," FJD"," FKP"," GBP"," GEL"," GHS"," GIP"," GMD"," GNF"," GTQ"," GWP"," GYD"," HKD"," HNL"," HRK"," HTG"," HUF"," IDR"," ILS"," INR"," IQD"," IRR"," ISK"," JMD"," JOD"," JPY"," KES"," KGS"," KHR"," KMF"," KPW"," KRW"," KWD"," KYD"," KZT"," LAK"," LBP"," LKR"," LRD"," LSL"," LTL"," LVL"," LYD"," MAD"," MDL"," MGA"," MKD"," MMK"," MNT"," MOP"," MRO"," MUR"," MVR"," MWK"," MXN"," MYR"," MZE"," MZN"," NAD"," NGN"," NIO"," NOK"," NPR"," NZD"," OMR"," PAB"," PEN"," PGK"," PHP"," PKR"," PLN"," PYG"," QAR"," RON"," RSD"," RUB"," RWF"," SAR"," SBD"," SCR"," SDG"," SEK"," SGD"," SHP"," SKK"," SLL"," SOS"," SRD"," SSP"," STD"," SVC"," SYP"," SZL"," THB"," TJS"," TMT"," TND"," TOP"," TRY"," TTD"," TWD"," TZS"," UAH"," UGX"," USD"," UYU"," UZS"," VEF"," VND"," VUV"," WST"," XAF"," XCD"," XOF"," XPF"," YER"," ZAR"," ZMW"
	};
	
	public static String[] CurrencySymbl={
			"AED","AFN","ALL","AMD","ANG","AOA","ARS","A$","AWG","AZN","BAM","BBD","BDT","BGN","BHD","BIF","BMD","BND","BOB","R$","BSD","BTN","BWP","BYR","BZD","CA$","CDF","CHF","CLP","CN¥","COP","CRC","CUC","CUP","CVE","CZK","DJF","DKK","DOP","DZD","EGP","ERN","ETB","€","FJD","FKP","£","GEL","GHS","GIP","GMD","GNF","GTQ","GWP","GYD","HK$","HNL","HRK","HTG","HUF","IDR","₪","₹","IQD","IRR","ISK","JMD","JOD","¥","KES","KGS","KHR","KMF","KPW","₩","KWD","KYD","KZT","LAK","LBP","LKR","LRD","LSL","LTL","LVL","LYD","MAD","MDL","MGA","MKD","MMK","MNT","MOP","MRO","MUR","MVR","MWK","MX$","MYR","MZE","MZN","NAD","NGN","NIO","NOK","NPR","NZ$","OMR","PAB","PEN","PGK","PHP","PKR","PLN","PYG","QAR","RON","RSD","руб.","RWF","SAR","SBD","SCR","SDG","SEK","SGD","SHP","SKK","SLL","SOS","SRD","SSP","STD","SVC","SYP","SZL","฿","TJS","ТМТ","TND","TOP","TRY","TTD","NT$","TZS","₴","UGX","$","UYU","UZS","VEF","₫","VUV","WST","FCFA","EC$","CFA","CFPF","YER","ZAR","ZMW"
	};
	
	public static String[] CurrencyName_ru={
			"Дирхам ОАЭ","Афганский афгани","Албанский лек","Армянский драм","Нидерландский антильский гульден","Ангольская кванза","Аргентинское песо","Австралийский доллар","Арубанский флорин","Азербайджанский манат","Конвертируемая марка Боснии и Герцеговины","Барбадосский доллар","Бангладешская така","Болгарский лев","Бахрейнский динар","Бурундийский франк","Бермудский доллар","Брунейский доллар","Боливийский боливиано","Бразильский реал","Багамский доллар","Бутанский нгултрум","Ботсванская пула","Белорусский рубль","Белизский доллар","Канадский доллар","Конголезский франк","Швейцарский франк","Чилийское песо","Китайский юань","Колумбийское песо","Костариканский колон","Кубинское конвертируемое песо","Кубинское песо","Эскудо Кабо-Верде","Чешская крона","Франк Джибути","Датская крона","Доминиканское песо","Алжирский динар","Египетский фунт","Накфа","Эфиопский быр","Евро","Доллар Фиджи","Фунт Фолклендских островов","Английский фунт","Грузинский лари","Ганский седи","Гибралтарский фунт","Гамбийский даласи","Гвинейский франк","Гватемальский кетсаль","Песо Гвинеи-Бисау","Гайанский доллар","Гонконгский доллар","Гондурасская лемпира","Хорватская куна","Гаитянский гурд","Венгерский форинт","Индонезийская рупия","Новый израильский шекель","Индийская рупия","Иракский динар","Иранский риал","Исландская крона","Ямайский доллар","Иорданский динар","Японская иена","Кенийский шиллинг","Киргизский сом","Камбоджийский риель","Франк Коморских островов","Северокорейская вона","Южнокорейская вона","Кувейтский динар","Доллар Каймановых островов","Казахский тенге","Лаосский кип","Ливанский фунт","Шри-Ланкийская рупия","Либерийский доллар","Лоти","Литовский лит","Латвийский лат","Ливийский динар","Марокканский дирхам","Молдавский лей","Малагасийский ариари","Македонский динар","Мьянманский кьят","Монгольский тугрик","Патака Макао","Мавританская угия","Маврикийская рупия","Мальдивская руфия","Малавийская квача","Мексиканское песо","Малайзийский ринггит","Мозамбикское эскудо","Мозамбикский метикал","Доллар Намибии","Нигерийская найра","Никарагуанская кордоба","Норвежская крона","Непальская рупия","Новозеландский доллар","Оманский риал","Панамское бальбоа","Перуанский новый соль","Кина Папуа — Новой Гвинеи","Филиппинское песо","Пакистанская рупия",
			"Польский злотый","Парагвайский гуарани","Катарский риал","Румынский лей","Сербский динар","Российский рубль","Франк Руанды","Саудовский риал","Доллар Соломоновых островов","Сейшельская рупия","Суданский фунт","Шведская крона","Сингапурский доллар","Фунт острова Святой Елены","Словацкая крона","Леоне","Сомалийский шиллинг","Суринамский доллар","Южносуданский фунт","Добра Сант-Томе и Принсипи","Сальвадорский колон","Сирийский фунт","Свазилендский лилангени","Таиландский бат","Таджикский сомони","Туркменский новый манат","Тунисский динар","Тонганская паанга","Турецкая лира","Доллар Тринидада и Тобаго","Новый тайваньский доллар","Танзанийский шиллинг","Украинская гривна","Угандийский шиллинг","Доллар США","Уругвайское песо","Узбекский сум","Венесуэльский боливар","Вьетнамский донг","Вату Вануату","Самоанская тала","Франк КФА ВЕАС","Восточно-карибский доллар","Франк КФА ВСЕАО","Французский тихоокеанский франк","Йеменский риал","Южноафриканский рэнд","Замбийская квача"
	};
	
	public String TAG = "Capital";
	public String TAG_RESULT = "RESULT";
	public String TAG_ADD = "ADD";
	
	final int USD_RUB = 5754;
	final int EUR_RUB = 6797;
	final int RUB_RUB = 100;
	final int DECIMAL = 100;
	
	final int NUMB_SEL_ITEM = 4;
	
	final int CurrencySelectMaxSymbol = 19;
	
	final int MAX_NUMB_VALUE = 7;
	final int MAX_DEC_VALUE = 1;
	
	final int MAX_SPINER_ITEM = 3;
	
	String[] Spiner_Array = new String[]{"RUB","USD","EUR"};
	
	final String SAFE_SPINER="safe_spiner";  //Ключ для сохранения спинера выбора валюты для добавления капиталов
	final String SAFE_MAIN_SPINER="main_currency";//Ключ для сохранения спинера основной валюты
	String[] SpinerArray = new String[]{"USD","RUB","EUR","..."};
	final String KEY_MAIN_CURRENCY="main_currency";  //ключ для основной валюты

	final String EXTRA_DEPOSIT_NAME = "deposit_name";
	final String EXTRA_NUMB = "deposit_numb";
	final String EXTRA_CURRENCY = "deposit_currency";
	final String EXTRA_VALUE = "deposit_value";
	final String EXTRA_DATA_HAVE = "data_have";//при инициализации показывает были ли данные 
	
	final int REDRAW_LISTVIEW = 0; //пердача данных из класса диалога в основное активити
	final int ERR_PARSING_FLOAT= 1;
	final int ERR_PARSING_CURRENCY= 2;
	final int SAVE_COURSE= 3;//сохранить текущий курс валюты
	final int VIEW_RESULT= 4;//показываем результат
	final int START_REQUEST= 5;//режим частых запросов для отображения данных
	final int JENERAL_REQUEST= 6;//режим запросов запросов для отображения данных
	final int SAVE_PERIOD_REQUEST = 7;//сохранить период запросов
	final int REDRAW_DATE = 8;//сохранить период запросов
	final int SET_RESULT=9;	//устанавливаем точки в результате
	long StartTimer = 1000;//запуск таймера через 1 секунду
	long shortPeriodTimer = 10000;//для частых запросов короткий период 10сек
	int TimeOut = 5000;//5 сек
	
	int[] arrPeriodRequest = {(30*1000),(5*60*1000),(15*60*1000),(60*60*1000),(24*60*60*1000),(30*24*60*60*1000)};
	/*<item>30 сек.</item>
        <item>5 мин.</item>
        <item>15 мин</item>
        <item>1 час</item>
        <item>1 день</item>
        <item>никогда</item>*/
	
	String REQUEST_SERVER_1 = "http://rate-exchange.appspot.com/currency? from=USD&to=EUR";
	String  CONTENT_EXAMPLE = "{\"base\":\"RUB\",\"date\":\"2017-11-13\",\"rates\":{\"AUD\":0.022005,\"BGN\":0.02821,\"BRL\":0.055332,\"CAD\":0.02136,\"CHF\":0.016718,\"CNY\":0.11165,\"CZK\":0.36877,\"DKK\":0.10733,\"GBP\":0.01284,\"HKD\":0.13112,\"HRK\":0.1089,\"HUF\":4.5007,\"IDR\":228.05,\"ILS\":0.059565,\"INR\":1.1004,\"JPY\":1.9049,\"KRW\":18.823,\"MXN\":0.32234,\"MYR\":0.070468,\"NOK\":0.1369,\"NZD\":0.024343,\"PHP\":0.86143,\"PLN\":0.061082,\"RON\":0.067159,\"SEK\":0.14093,\"SGD\":0.022886,\"THB\":0.55581,\"TRY\":0.065365,\"USD\":0.016812,\"ZAR\":0.24475,\"EUR\":0.014424}}";
	
	String[] ArrOriginCurrency=new String[CurrencyCode.length];
	int count_origin_currency=0;
	
	int MAX_COUNT_CURRENCY = 50;
	int MAX_COUNT_UST = 10;
	
	String JSON_EXAMPLE = "{\"query\":	{\"count\":1,\"created\":\"2017-10-19T09:57:26Z\",\"lang\":\"en-US\",\"results\":{\"row\":{\"rate\":\"57.5555\",\"name\":\"USD/RUB\"}}}}";
	
	String STR_JSON_BEGIN = "{\"";
	String STR_JSON_END = ");";
	String STR_JSON_QUERY = "query";
	String STR_JSON_RESULTS = "results";
	String STR_JSON_ROW = "row";
	String STR_JSON_RATE = "rate";
	
	String KEY_COURSE = "course_exchange";
	String KEY_DATE_UPDATE = "course_date";
	String NOTHING = ",";
	String DATE_EXAMPLE = "Mon Oct 23 08:05:59 GMT+05:00 2017";
	String DATE_SIMPLE_FORMAT = "MM/dd/yyyy/HH/mm/ss";
	String DATE_SIMPLE_EXAMPLE = "03/26/2012";
	
	int CURRENCY_ARRAY_SIZE = 28;
	int CURRENCY_TYPE = 3;
	int MAX_LENHT_NAME_DEPOSIT = 9;
	int MAX_LENHT_SUM = 6;
	
	String CONTENT_SAVE	=	"content_save";
	String CONTENT_KEY	=	"content_key";
	
	//разбор json для единого банка европы
	
}
