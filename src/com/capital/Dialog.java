package com.capital;

import java.io.IOException;
import java.util.Currency;

import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
 
public class Dialog extends DialogFragment implements OnClickListener {
 
  String Text;
  int numbPos=0;
  DataStorage StoreData;
  Handler h;
  Context dialogContext;
  String dCurrency;
  String Value;
  
  Dialog(String mText,int mPos, Context mContext, Handler mHandler, String mCurrency, String mValue){
	  Text = mText;
	  numbPos = mPos;
	  StoreData = new DataStorage();
	  StoreData.setContent();
	  h = mHandler;
	  dialogContext = mContext;
	  Value = mValue;
	  dCurrency = mCurrency;
  }
  
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      
   Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.dialog, null);
    TextView vText =(TextView) v.findViewById(R.id.tText);
    vText.setText(Text);
    v.findViewById(R.id.bDelete).setOnClickListener(this);
    v.findViewById(R.id.bChange).setOnClickListener(this);
    return v;
  }
 
  public void onClick(View v) {
    Log.d(iCapital.TAG, "Dialog: " + ((Button) v).getText());
    String nameButton =(String)((Button) v).getText();
    if (nameButton.equals(getResources().getString(R.string.Delete))){
    	StoreData.deleteDeposite(numbPos);
    }else if (nameButton.equals(getResources().getString(R.string.Change))){
    	Intent intent = new Intent(dialogContext, AddDeposit.class);
    	intent.putExtra(iCapital.EXTRA_DEPOSIT_NAME,Text);
    	intent.putExtra(iCapital.EXTRA_NUMB,String.valueOf(numbPos));
    	intent.putExtra(iCapital.EXTRA_CURRENCY,dCurrency);
    	intent.putExtra(iCapital.EXTRA_VALUE,Value);
    	startActivity(intent);	
    }
    h.sendEmptyMessage(iCapital.REDRAW_LISTVIEW);
    dismiss();
  }
 
  public void onDismiss(DialogInterface dialog) {
    super.onDismiss(dialog);
    Log.d(iCapital.TAG, "Dialog: onDismiss");
  }
 
  public void onCancel(DialogInterface dialog) {
    super.onCancel(dialog);
    Log.d(iCapital.TAG, "Dialog: onCancel");
  }
}
