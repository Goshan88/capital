package com.capital;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

public class ServiceCapital extends Service{
	
	private Timer serviceTimer;
	private ServiceTimerTask ServiceTask;	
	public static Handler servicHandler;
	public static boolean frequent_request = false;
	public static boolean timer_run = false;
	
	@Override
	public IBinder onBind(Intent intent){
		Log.d(iCapital.TAG, "Service onBinder");
		return null;
	}
	
	public void onCreate(){
		Log.d(iCapital.TAG, "Service onCreate");
			serviceTimer = new Timer();
			ServiceTask = new ServiceTimerTask();
			serviceTimer.schedule(ServiceTask, iCapital.StartTimer, iCapital.shortPeriodTimer);
			frequent_request = true;
			
		servicHandler = new Handler(){
       	  public void handleMessage(android.os.Message msg) {
       		if (msg.what == iCapital.START_REQUEST){//������ �������� �����
       			if(timer_run){
       				serviceTimer.cancel();
       				ServiceTask.cancel();
       				timer_run = false;
       			}
       			serviceTimer = new Timer();
       			ServiceTask = new ServiceTimerTask();
       			serviceTimer.schedule(ServiceTask, iCapital.StartTimer, iCapital.shortPeriodTimer);
       			frequent_request = true;
       			Log.d(iCapital.TAG, "Short period request ");
       		  }if (msg.what == iCapital.JENERAL_REQUEST){//����������� ����� �������
       			  	String[] arrTmp = MyContext.getContext().getResources().getStringArray(R.array.PeriodUpdate);     			  	
       			 if(timer_run){
       				 serviceTimer.cancel();
       				 ServiceTask.cancel();
       				 timer_run = false;
       		  		}
       			 int newPeriod= iCapital.arrPeriodRequest[3];//������������� ������ ������� � 1 ���
       			 serviceTimer = new Timer();
       			 ServiceTask = new ServiceTimerTask();
   				 serviceTimer.schedule(ServiceTask, iCapital.StartTimer, newPeriod);
       			 frequent_request = false;
       			 Log.d(iCapital.TAG, "Set period request " + " Set tics: 1hour");
       		  		
       		  }if (msg.what == iCapital.VIEW_RESULT){//�������� ����� �� ������� ��������
       			MainView.drawResult();
       		  }
       	  };
       };
       
       super.onCreate();
	}
	
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(iCapital.TAG, "Service onStartCommand");
		return super.onStartCommand(intent, flags, startId);
	}
	
	public void onDestroy() {
		super.onDestroy();
		Log.d(iCapital.TAG, "Service onDestroy");
	}
	
}

class ServiceTimerTask extends TimerTask {
				
	@Override
	public void run(){
		ServiceCapital.timer_run = true;
		Log.d(iCapital.TAG,"Periodic Service Task");
		
		String mainCurrency = DataStorage.loadMainCurrency();
		String content = getContetn(mainCurrency);

		if (!(content.equals(iCapital.NOTHING))){
			Log.d(iCapital.TAG, "content.equals(iCapital.NOTHING)");
			DataStorage.saveContent(content,MyContext.getContext());
			ServiceCapital.servicHandler.sendEmptyMessage(iCapital.VIEW_RESULT);
			if (ServiceCapital.frequent_request == true){
				Log.d(iCapital.TAG, "frequent_request == true");
				ServiceCapital.servicHandler.sendEmptyMessage(iCapital.JENERAL_REQUEST);
			}
		}
	}	
	
	public String getContetn(String baseCurrency){
		try{
			URL obj = new URL("http://api.fixer.io/latest?base="+baseCurrency);
			Log.d(iCapital.TAG,"Service getContent: URL: "+obj.toString());
			HttpURLConnection con = (HttpURLConnection)obj.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("User-agent", "Mozilla/5.0");
			con.setRequestProperty("Accept-Charset", "UTF-8");
			con.setReadTimeout(iCapital.TimeOut);//5sec
			InputStream response = con.getInputStream();
			Scanner s = new Scanner(response).useDelimiter("\\A");
			String result = s.hasNext() ? s.next() : "";
			Log.d(iCapital.TAG,"Service getContent: return-" + result);
			return result;
		}catch(Exception e){
			Log.d(iCapital.TAG, "Error get Content: " + e.toString());
			return iCapital.NOTHING;
		}
	
	}
}



