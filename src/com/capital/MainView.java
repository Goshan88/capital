package com.capital;

import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class MainView extends Activity{

	TextView tNumb, tResult;
	public static TextView tValue;
	
	ArrayList<Map<String, Object>> data;
	Map<String, Object>m;
	final String ATTRIBUTE_COUNT_TEXT = "count";
	final String ATTRIBUTE_NAME_TEXT = "name";
	final String ATTRIBUTE_VALUE_TEXT = "value";
	final String ATTRIBUTE_VALUE_CURRENCY = "currency";

	private String[] Name;
	private int[] Value;
	private String[] Symbol;
	private String[] lCurrency;
	Context Context;
	
	 MainView(Context mContext,Deposit[] mDeposite){
		 Context = mContext;
		 
		 int[] tmpValue = new int[mDeposite.length];
		 String[] tmpName = new String[mDeposite.length];
		 String[] tmpSymbol = new String[mDeposite.length];
		 String[] tmpCurrency = new String[mDeposite.length];
		 
		 for(int i=0;i<mDeposite.length;i++){
			Deposit tmpDeposit = mDeposite[i];
			tmpValue[i] = tmpDeposit.getValue();
			tmpName[i]	 =  tmpDeposit.getName();
			tmpSymbol[i]   =  myCurrency.getSymbol(tmpDeposit.getCurrency());
			tmpCurrency[i]  =  tmpDeposit.getCurrency().toString();
		 }
		 Value = tmpValue;
		 Name = tmpName;
		 Symbol = tmpSymbol;
		 lCurrency = tmpCurrency;
	 }
	 
	 public void view(ListView mView){
		
        tResult = (TextView)((Activity)Context).findViewById(R.id.tResult);
       
        float sum=0;//����� �� ������
        
		data = new ArrayList<Map<String,Object>>();
		for(int i=0; i<Name.length; i++){
			m = new HashMap<String, Object>();
			m.put(ATTRIBUTE_COUNT_TEXT,String.valueOf((i+1))+".");
			m.put(ATTRIBUTE_NAME_TEXT,Name[i]);

			sum = sum + Value[i];
			
			m.put(ATTRIBUTE_VALUE_TEXT,String.valueOf(Value[i])+ Symbol[i]);
			
			m.put(ATTRIBUTE_VALUE_CURRENCY,lCurrency[i]);
			data.add(m);
		}
		
		String[] from = { ATTRIBUTE_COUNT_TEXT, ATTRIBUTE_NAME_TEXT,ATTRIBUTE_VALUE_TEXT};
		int[] to = {R.id.tvCount, R.id.tvName, R.id.tvValue};
		SimpleAdapter sAdapter = new SimpleAdapter(Context, data, R.layout.item, from, to);
		mView.setAdapter(sAdapter);
		
		drawResult();
	   }
	 
	 
	 public static void drawResult(){
		 float result=0,tmp=0;
		 float rate=0;
		 boolean get_new_request = false;
		 
		 DataStorage DepositLoad = new DataStorage();
		 Deposit[] DepositList = DepositLoad.init();
		 TextView resValue = (TextView)((Activity)MyContext.getContext()).findViewById(R.id.tValue);
		 
		 //��������� ���� ������� ������ �� ��������� �� ��������������� ������
		String load_currency = DataStorage.loadMainCurrency();
		String json_currency = JSONparsing.getMainCurrency(); 
		 Log.d(iCapital.TAG, "DataStorage.loadMainCurrency():" + load_currency + 
				 ", JSONparsing.getMainCurrency():" + json_currency);
		 try{
			 if(load_currency.equals(json_currency)){
				 get_new_request = false;
			 }else{
				 get_new_request = true;
			 }
		 }catch(Exception e){
			 Log.d(iCapital.TAG, "Erro equal two string: " + e.getMessage());
		 }
		 Log.d(iCapital.TAG, "get_new_request: " + String.valueOf(get_new_request));
		 
		if (get_new_request){
			 resValue.setText("....");
			 //ServiceCapital.servicHandler.sendEmptyMessage(iCapital.START_REQUEST);//������������ ���������� ����������
			 Log.d(iCapital.TAG, "draw result start freq request");
			 return;
		 }
		 
		 //���� �� �������
		 for(int i=0; i<DepositList.length; i++){
			 //�������� �������� ����� ������
			  rate =  JSONparsing.getRate(DepositList[i].getCurrency());
			 try{
				 rate = 1 / rate;
			 }catch(Exception e){
				 Toast.makeText(MyContext.getContext(), (MyContext.getContext().getResources().getString(R.string.Err_devision_at_zero)), Toast.LENGTH_LONG).show();
				 Log.d(iCapital.TAG,MyContext.getContext().getResources().getString(R.string.Err_devision_at_zero)+" " + e.getMessage()); 
			 }
			 tmp  = (float)DepositList[i].getValue() * rate;
			 result = result + tmp;
			 Log.d(iCapital.TAG,"Deposite Currency:" + DepositList[i].getCurrency() + " Currency rate:" + rate
					 + " Value deposit:" + String.valueOf(DepositList[i].getValue()) + "Change summ:" + tmp + " Summ:" + String.valueOf(result));
		 }
		  
		 String st_result = String.format("%.2f", result) + myCurrency.getSymbol(JSONparsing.getMainCurrency());
		 resValue.setText(st_result);
	}
		
}
