package com.capital;

import android.content.Context;

public class MyContext {

	private static Context cContext;
	
	MyContext(Context mContext){
		cContext = mContext;
	}
	
	public static Context getContext(){
		return cContext;
	}
}
